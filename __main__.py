import base64
import os
import sys
from enum import Enum
from io import BytesIO

from PIL import Image

__dir__ = os.path.dirname(os.path.abspath(__file__))
__root__ = os.path.dirname(__dir__)
if __dir__ not in sys.path:
    sys.path.append(__dir__)
if __root__ not in sys.path:
    sys.path.append(__root__)
os.chdir(__root__)

from email_flow.account import yield_account
import random
import time

import requests
from selenium.webdriver.common.keys import Keys
import selenium.common.exceptions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import pymongo
from pymongo import MongoClient

from MyBrowser import HeadlessChrome, PATHS_CHROME

MAIL_MISSION_URL = "http://v.16mnc.cn:8088/version/MongodbMailMission/queryMail"
MAIL_MISSION_CALLBACK_URL = "http://v.16mnc.cn:8088/version/MongodbMailMission/returnMail"
MISSION_TEST = {'msg': '获取成功',
                'mail': {'vAddressee': '1347161021@qq.com\n1365160708@qq.com\n1372012887@qq.com\n1419663910@qq.com\n',
                         'vContent': '大都在苹果手机来搜索下载APP:邻家商品 看商品赚零花钱 每天30分钟刷出3到5元 提现秒到帐 、-..、、', 'vGroup': 2, 'vId': 92,
                         'vTitle': '你好，幸运的 %s，在这里输入您的主题'}, 'status': 200}


###

class AccountStatus(Enum):
    normal = 0  # 政策
    needVerification = 1  # 需要验证码识别
    blocked = 2  # 被封


###

class Session(object):
    def __init__(self):
        self._con = MongoClient(os.getenv('URL_MONGO', 'mongodb://192.168.2.100:27017/'), connect=False)
        self._db = self._con['emailFlow']
        self._db2 = self._con['fake_flow']

        self._check_index()
        self.__i_yield_mission = self.__yield_mission()

    def _check_index(self):
        self.cookies.create_indexes([
            pymongo.IndexModel(
                [
                    ('uid', pymongo.ASCENDING),
                ],
                unique=True,
            ),
        ])

        self.done.create_indexes([
            pymongo.IndexModel(
                [
                    ('vId', pymongo.ASCENDING),
                ],
                unique=True,
            ),
        ])

    @property
    def cookies(self) -> pymongo.collection.Collection:
        return self._db.cookies

    @property
    def done(self) -> pymongo.collection.Collection:
        return self._db.done

    @property
    def mission(self) -> pymongo.collection.Collection:
        return self._db2.mission_email

    @property
    def account(self) -> pymongo.collection.Collection:
        return self._db.account

    def save_cookies(self, cookies):
        self.cookies.insert_one(cookies)

    @property
    def next_mission(self):
        return next(self.__i_yield_mission)

    @property
    def next_account(self):
        for account in self.account.find_one({}):
            yield account

    def finish_mission(self, v_id):
        self.done.update_one({"vId": v_id}, {"$inc": {"vDone": 1}}, upsert=True)

    def get_mission_done_count(self, v_id):
        m = self.done.find_one({"vId": v_id})
        if m:
            return m['vDone']
        return 0

    def __yield_mission(self):
        while True:
            for mission in self.mission.find({'vStatus': 1}):
                yield mission


try:
    Session = Session()
except pymongo.errors.ServerSelectionTimeoutError:
    print("pymongo.errors.ServerSelectionTimeoutError")
    pass
###

MAIL_INDEX = "https://mail.163.com/"


class NetEaseMailBrowser(HeadlessChrome):
    # class NetEaseMailBrowser(HeadlessFirefox):
    def __init__(self, **kwargs):
        PATHS_CHROME.update(kwargs)
        kwargs = PATHS_CHROME
        # kwargs.setdefault("proxy", "192.168.2.106:3128")
        kwargs.setdefault("rect", {
            'x': 0,
            'y': 0,
            'width': 1080,
            'height': 720,
        }, )

        super(NetEaseMailBrowser, self).__init__(**kwargs)
        self.__wait__ = WebDriverWait(self, 20, 0.5)

    def __enter__(self):
        self.implicitly_wait(10)
        self.get(MAIL_INDEX)
        self.wait_til_complete()
        return super(NetEaseMailBrowser, self).__enter__()

    def _dump(self):
        """存储当前 cookies 到数据库"""

        if self.status_login:
            uid = self.cookies['mail_uid'].strip("'")
            Session.cookies.update_one(
                {
                    "uid": uid,
                },
                {
                    "$set": {
                        "uid": uid,
                        'cookies': self.get_cookies(),
                    }
                },
                upsert=True,
            )

    def wait_til_complete(self):
        while True:
            status = self.execute_script("return document.readyState")
            if status != 'completed':
                return
            else:
                time.sleep(.5)
                print(status)

    @property
    def cookies(self):
        return {cookie['name']: cookie['value'] for cookie in self.get_cookies()}

    @property
    def status_login(self):
        # self.wait_til_complete()

        if 'mail_uid' in self.cookies and self.find_elements_by_css_selector("#spnUid"):
            return True
        else:
            return False

        # if self.current_url == MAIL_INDEX:
        #     pass
        # else:
        #     raise NotImplementedError("不是首页目前无从判断")
        # return True

    def click_element_by_css_selector(self, css):
        try:
            self.find_element_by_css_selector(css).click()
        except selenium.common.exceptions.ElementClickInterceptedException:
            print()
            pass

    def login(self, uid, passwd):
        def wait_span_uid():
            self.switch_to.default_content()

            if self.find_elements_by_css_selector("span#spnUid"):
                self.wait_til_complete()

            time.sleep(1)

        try:
            session_data = Session.cookies.find_one({"uid": uid})
            if session_data:
                for cookie in session_data['cookies']:
                    self.add_cookie(cookie)
                self.refresh()

                wait_span_uid()
                self._dump()

                if self.status_login:
                    return True

            lbNormal = self.__wait__.until(lambda x: x.find_element_by_css_selector("#lbNormal"))
            action = ActionChains(self)
            action.move_to_element(lbNormal).click().perform()
            self.switch_to.frame(self.find_element_by_css_selector("iframe[frameborder='0'][name]"))

            input_email = self.__wait__.until(lambda x: x.find_element_by_css_selector("input.dlemail"))
            input_email.send_keys(uid.split('@')[0])

            input_passwd = self.find_element_by_css_selector("input.dlpwd")
            input_passwd.send_keys(passwd)

            # 十天免登陆
            self.find_element_by_css_selector("div.b-unlogn.j-unlogn > label[for='un-login']").click()

            input_passwd.send_keys(Keys.ENTER)
            time.sleep(0.8)

            self.switch_to.default_content()
            self.switch_to.frame(self.find_element_by_css_selector("iframe[frameborder='0'][name]"))
            button_goon = self.find_elements_by_css_selector("a[data-action=modeGoon]")
            if button_goon:
                button_goon[0].click()

            wait_span_uid()

            self._dump()
        except:
            pass

    def send(self, to_="3099891314@qq.com", title="账号测试", content="呵呵，"):
        self.__wait__.until(lambda x: x.find_element_by_css_selector("li[title*=首页] > div.nui-fNoSelect")).click()
        self.__wait__.until(lambda x: x.find_element_by_css_selector("#dvNavTop > ul > li:nth-child(2)")).click()

        for i in range(5):
            eles = self.find_elements_by_css_selector("a.APP-editor-btn-switchSource")
            if eles:
                break
            editorcmd_full_eles = self.find_elements_by_css_selector("a[editorcmd=full]")
            if editorcmd_full_eles[0]:
                editorcmd_full_eles[0].click()
                break
            time.sleep(1)
        time.sleep(1)

        self.find_element_by_css_selector("div.js-component-emailcontainer input").send_keys(to_)
        self.find_element_by_css_selector("div.js-component-input input[maxlength='256']").send_keys(title)

        eles = self.find_elements_by_css_selector("a.APP-editor-btn-switchSource")
        eles[0].click()

        self.execute_script(
            "arguments[0].value = arguments[1];",
            self.find_element_by_css_selector("textarea.APP-editor-textarea"),
            content,
        )

        self.find_elements_by_css_selector("a.APP-editor-btn-switchSource")[1].click()

        for span in self.find_elements_by_css_selector("header.frame-main-cont-head > div[role=toolbar] > "
                                                       ".nui-toolbar-item > div[tabindex='1'] > span.nui-btn-text"):
            if span.text == '发送':
                span.click()

        self.wait_til_complete()

        for i in range(5):
            eles = self.find_elements_by_css_selector("b.nui-ico.se0.pv1")
            if eles:
                self.find_element_by_css_selector("li[aria-selected=true] > a").click()  # 关闭已发邮件的标签
                return True

            time.sleep(random.randint(50, 200) / 100)
            eles = self.find_elements_by_css_selector("div.nui-msgbox-hd")
            if eles:
                img_verify = self.find_elements_by_css_selector("#imgMsgBoxVerify")[0]
                left = img_verify.location['x']
                top = img_verify.location['y']
                right = img_verify.location['x'] + img_verify.size['width']
                bottom = img_verify.location['y'] + img_verify.size['height']

                self.save_screenshot('bgimg.png')
                bgimg = Image.open('bgimg.png')
                verify_img = bgimg.crop((left, top, right, bottom))
                guess_str = self.gen_img_verify_str(verify_img)
                input_verify = self.find_elements_by_css_selector("td.nui-form-cont > div > input")[0]
                input_verify.send_keys(guess_str)
                time.sleep(0.3)
                input_verify.send_keys(Keys.ENTER)

            time.sleep(1)

        time.sleep(0.8)

    def gen_img_verify_str(self, img):
        output_buffer = BytesIO()
        img.save(output_buffer, format='PNG')
        byte_data = output_buffer.getvalue()
        encoded_string = base64.b64encode(byte_data).decode()
        resp = requests.get("http://192.168.2.182:19730/|WY|" + encoded_string)
        return resp.text


def insert_hide_dirty_content_tag(content):
    tags = ["p", "b", "strong", "font", "s", "em", "i", "h1", "h2", "h3", "span", "div"]
    tag = random.choice(tags)
    str = f"""<{tag} hidden="hidden">{next(rand_str)}</{tag}>"""
    for char in content:
        if char.isdigit() or char.islower() or char.isupper():
            str += char + f"""<{tag} hidden="hidden">{next(rand_str)}</{tag}>"""
        else:
            tag = random.choice(tags)
            str += char + f"""<{tag} hidden="hidden">{next(rand_str)}</{tag}>"""
    return str


def dirty_content(
        title="亲爱的威武小草",
        content="专業邮件代發, 接各种推广, 引流, 兼职, 菠菜, 精准, 稳定收件箱, 需要联系qq: 9529656。一次投入，终生受益",

        sign="打擾请见谅！信誉成就一切！"):
    content = ''.join(insert_hide_dirty_content_tag(content))
    content_wrap = f"""<div style="line-height:1.7;color:#000000;font-size:14px;font-family:Arial">
    <div style="line-height: 1.7;"><div style="line-height: 1.7;"><div style="line-height: 1.7;">
    <div style="color: rgb(0, 0, 0); font-family: Arial; font-size: 14px; border-width: 1px; 
    border-style: solid; border-color: rgb(200, 207, 218); padding: 40px;">{title}：</div>
    <div style="border-width: 1px; border-style: solid; border-color: rgb(200, 207, 218); padding: 40px;"><div class="nui-scroll" style="">
    <span style="color: rgb(255, 0, 0); font-size: 24px;">{content}
    </span></div></div><div style="color: rgb(102, 102, 102); font-family: &quot;lucida Grande&quot;, Verdana, 
    &quot;Microsoft YaHei&quot;; font-size: 12px; border-width: 1px; border-style: solid; border-color: rgb(204, 204, 204) 
    rgb(200, 207, 218) rgb(200, 207, 218); padding: 10px 0px; margin: 20px 0px;"><div><br /></div>
    {sign}
    </div></div></div></div></div>

    """
    return content_wrap


def yield_nickname():
    l = [
        "阿尔卑斯",
        "阿莫西林",
        "哀而不伤",
        "埃菲尔",
        "爱不单行",
        "爱不复生",
        "爱不离手",
        "爱的渡口",
        "爱的寄语",
        "爱的味觉",
        "爱断情伤",
        "爱多深",
        "爱疯了",
        "爱还逝",
        "爱很美",
        "爱狠无奈",
        "爱恨忐忑",
        "爱恒动",
        "爱久弥新",
        "爱落空",
        "爱旅途",
        "爱你",
        "爱你很多",
        "爱你依旧",
        "爱飘荡",
        "爱琴海",
        "爱情",
        "爱上香橙",
        "爱太累",
    ]
    random.shuffle(l)

    while True:
        for n in l:
            yield n


def random_str_from_txt():
    count = 0
    s = ""
    for line in open(f"{os.path.abspath('.')}/emailFlow/dirty", "r", encoding='utf-8'):
        for str in line[:-1]:
            if str >= u"\u4e00" and str <= u"\u9fff":
                if count < random.randint(2, 8):
                    s += str
                    count += 1
                else:
                    s += str
                    yield s
                    s = ""
                    count = 0


def random_str(num=2):
    while True:
        str = ""
        for _ in range(num):
            head = random.randint(0xb0, 0xf7)
            body = random.randint(0xa1, 0xf9)
            val = f'{head:x}{body:x}'
            str += bytes.fromhex(val).decode('gb2312')
        yield str


yield_nickname = yield_nickname()
rand_str = random_str()


def send_email(user, passwd, title, content, address_to):
    content = dirty_content(
        title="亲爱的幸运用户",
        content=content,
    )
    title = f"你好，{next(rand_str)}"
    resp = requests.get("http://192.168.2.100:5030/proxy/newest")
    proxy = resp.text

    # resp = requests.get(
    #     "http://api.wandoudl.com/api/ip?app_key=b1220eab752fa2aff1a22c8fbbef75d1&pack=1794&num=1&xy=1&type=2&lb=\r\n&mr=1&port=0")
    # j = resp.json()
    # proxy = j['data'][0]['ip'] + ":" + str(j['data'][0]['port'])
    # print(proxy)

    with NetEaseMailBrowser(proxy=proxy) as browser:
        browser.login(user, passwd)

        if browser.status_login:
            browser.send(
                title=title,
                content=content,
                to_=address_to,
            )

        print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), f"--{user}:{passwd}--{proxy}--在线",
              browser.status_login,
              title, address_to)
        return browser.status_login


def email_task(user, passwd, title, content, address_to):
    res = send_email(user, passwd, title, content, address_to)
    for _ in range(5):
        if res:
            return True
        else:
            res = send_email(user, passwd, title, content, address_to)


###


if __name__ == '__main__':
    while True:
        try:
            resp = requests.post(url=MAIL_MISSION_URL)
            mission = resp.json()
            # mission = MISSION_TEST
        except requests.exceptions.ConnectionError:
            print("无法获取任务，请检查网络")
            time.sleep(5 * 60)
            continue
        if "status" in mission and mission['status'] == 400:
            print(mission['msg'])
            time.sleep(5 * 60)
            continue
        v_id = mission['mail']['vId']
        v_group = mission['mail']['vGroup']
        v_title = mission['mail']['vTitle']
        v_content = mission['mail']['vContent']

        email_adrresses_content = mission['mail']['vAddressee']
        email_adrresses = email_adrresses_content.split("\n")

        # for address in email_adrresses:
        #     u, p = ("zhoueut31@163.com", "222222z")
        #     # u, p = ("mabr275@163.com", "v777777")
        #     # v_title = next(yield_nickname)
        #     send_email(v_id, **dict(
        #         user=u,
        #         passwd=p,
        #         title=v_title,
        #         content=v_content,
        #         address_to=address,
        #     ))

        from concurrent.futures import ThreadPoolExecutor

        with ThreadPoolExecutor(max_workers=5) as ex:
            for address in email_adrresses:
                if '@' not in address:
                    continue
                u, p = next(yield_account)

                ex.submit(email_task, **dict(
                    user=u,
                    passwd=p,
                    title=v_title,
                    content=v_content,
                    address_to=address,
                ))

        try:
            requests.post(url=MAIL_MISSION_CALLBACK_URL,
                          data={
                              'vId': v_id,
                              'vGroup': v_group,
                          })
            os.system("ll /tmp/ | grep .org | xargs rm -rf")
            os.system("ll /tmp/ | grep .com | xargs rm -rf")
        except requests.exceptions.ConnectionError:
            print("无法提交任务，请检查网络")
            continue
